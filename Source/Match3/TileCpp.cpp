// Fill out your copyright notice in the Description page of Project Settings.


#include "TileCpp.h"

// Sets default values
ATileCpp::ATileCpp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Points = 1.f;
}

// Called when the game starts or when spawned
void ATileCpp::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATileCpp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATileCpp::AddPoints_Implementation(float Count)
{
	Points += Count;
}

void ATileCpp::SelectTile()
{
	if (IsValid(GridOwner))
	{
		GridOwner->OnTileSelected(this);
	}
}

void ATileCpp::MoveEnd(ATileCpp * Tile)
{
	if (IsValid(GridOwner))
	{
		GridOwner->MoveComplite(this);
	}
}

void ATileCpp::SetNewForm(EFormTile NewForm)
{
	Info.Form = NewForm;
	RefreshInfo();
}

