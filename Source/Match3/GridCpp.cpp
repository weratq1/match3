// Fill out your copyright notice in the Description page of Project Settings.


#include "GridCpp.h"
#include "Engine/World.h"
#include "TileCpp.h"

#include "Math/UnrealMathUtility.h"

// Sets default values
AGridCpp::AGridCpp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MainState = EGridState::Idle;
	MinimumRunLength = 3;
}

// Called when the game starts or when spawned
void AGridCpp::BeginPlay()
{
	//GridTiles.Init(nullptr, 200);
	Super::BeginPlay();
	
}

// Called every frame
void AGridCpp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

ATileCpp * AGridCpp::GetTileByAdress(int x, int y)
{

	if (x >= 0 && y >= 0 && x <= 50 && y <= 50)
	{
		return Grid[x][y];
	}
	return nullptr;
}

void AGridCpp::SetTileInAdress(ATileCpp* Tile, int x, int y)
{	
	if (x >= 0 && y >= 0 && x <= 50 && y <= 50)
	{
		Grid[x][y]= Tile;
	}
}

void AGridCpp::GenerateGrid(int StartX, int StartY, int LengthX, int LengthY)
{
	XStart = StartX;
	YStart = StartY;
	CurrGridWidth = LengthX;
	CurrGridHeight = LengthY;
	xOffsetStart = StartX;
	yOffsetStart = StartY;
	xOffset = StartX;
	yOffset = StartY;
	GroundLevel = StartY;


	for (int ix = xOffsetStart; ix <= xOffsetStart + XStart; ++ix)
	{
		for (int iy = yOffsetStart; iy <= yOffsetStart + YStart; ++iy)
		{
			FTileInfo NewTile = GetTileForXY(ix,iy);
			SpawnTile(ix, iy,NewTile);
		}
	}
}

FTileInfo AGridCpp::GetTileForXY(int x, int y)
{
//return GenerateRandomTile();

	TArray<FTileInfo> ArrToCheck;
	for(int i = 0; i<10; ++i)
	{	
		ArrToCheck.Empty();

		FTileInfo CurrInfo = GenerateRandomTile();
		ArrToCheck.Add(CurrInfo);
		
		if (x-1>0 && y-1>0 && x+1<50 && y+1<50 && IsValid(GetTileByAdress(x - 1,y)))
		{
			ArrToCheck.Add(GetTileByAdress(x - 1, y)->Info);
		}
		if (x - 1 > 0 && y - 1 > 0 && x + 1 < 50 && y + 1 < 50 && IsValid(GetTileByAdress(x + 1,y)))
		{
			ArrToCheck.Add(GetTileByAdress(x + 1, y)->Info);
			
		}
		if (x - 1 > 0 && y - 1 > 0 && x + 1 < 50 && y + 1 < 50 && IsValid(GetTileByAdress(x,y + 1)))
		{
			ArrToCheck.Add(GetTileByAdress(x, y + 1)->Info);
		}
		if (x - 1 > 0 && y - 1 > 0 && x + 1 < 50 && y + 1 < 50 && IsValid(GetTileByAdress(x,y - 1)))
		{
			ArrToCheck.Add(GetTileByAdress(x, y - 1)->Info);
		}
		bool test = IsDiffrentTiles(ArrToCheck,true,true);

		if (test)
		{
			return CurrInfo;
		}
	}
	return GenerateRandomTile();
}

bool AGridCpp::IsDiffrentTiles(TArray<FTileInfo> Tiles, bool UseColor, bool UseForm)
{	
	if(!Tiles.IsValidIndex(1)) {return true;};
	bool Result = true;
	FTileInfo StartTile = Tiles[0];
	for(int i = 1; i < Tiles.Num(); ++i)
	{
		
		if (UseColor)
		{
			Result = Tiles[i].Color != StartTile.Color;
		}

		if (Result == false)
		{
			return false;
		}

		if (UseForm)
		{
			Result = Tiles[i].Form != StartTile.Form;
		}

		if (Result == false)
		{
			return false;
		}
	}
	return true;
}

FTileInfo AGridCpp::GenerateRandomTile()
{
	FTileInfo RandomInfo;
	RandomInfo.Color = ListColors[FMath::RandRange(0, ListColors.Num() - 1)];
	RandomInfo.Form = ListForm[FMath::RandRange(0, ListForm.Num() - 1)];
	
	return RandomInfo;
}

void AGridCpp::SpawnTile(int x, int y, FTileInfo InInfo)
{
	FActorSpawnParameters SpawnInfo;
	
	ATileCpp* CurrTile = GetWorld()->SpawnActor<ATileCpp>(TileClass, FVector(x*10.f,y*-10.f, GetActorLocation().Z), FRotator::ZeroRotator, SpawnInfo);
	CurrTile->Info = InInfo;
	CurrTile->GridOwner = this;
	CurrTile->XAdress = x;
	CurrTile->YAdress = y;
	Grid[x][y] = CurrTile;
	//SetTileInAdress(CurrTile,x,y);
}

void AGridCpp::OnTileSelected(ATileCpp* Tile)
{
	if (IsValid(SelectedTile))
	{
		if (SelectedTile->Info.Form == EFormTile::Bomb && SelectedTile == Tile)
		{
			ExecuteBomb(SelectedTile->XAdress, SelectedTile->YAdress,0,0, SelectedTile);
			//Grid[SelectedTile->XAdress][SelectedTile->YAdress] = nullptr;
			SelectedTile->Destroy();
			return;
		}
		SecondTile = Tile;
		if (CanSwipeTiles(SelectedTile, Tile))
		{
			StartSwipeElems(Tile);
			return;
		}
		else
		{
			Tile->TileDeselected();
			SelectedTile->TileDeselected();
			SelectedTile = nullptr;
			SecondTile = nullptr;
			return;
		}
	}
	SelectedTile = Tile;
	SelectedTile->TileSelected();
}

void AGridCpp::StartSwipeElems(ATileCpp * SecondElem)
{
if(!IsValid(SelectedTile) || !IsValid(SecondElem)){return;};
	int SelecX;
	int SelecY;
	int SecondX;
	int SecondY;

	GetTileXY(SelectedTile, SelecX, SelecY);
	GetTileXY(SecondElem, SecondX, SecondY);

	SetTileInAdress(SecondElem,SelecX,SelecY);
	SetTileInAdress(SelectedTile,SecondX,SecondY);

	MoveTileToNewId(SelectedTile,SecondX,SecondY);
	MoveTileToNewId(SecondElem, SelecX,SelecY);
	Grid[SelecX][SelecY] = SecondElem;
	Grid[SecondX][SecondY] = SelectedTile;
	


	MainState = EGridState::SwipeStarted;
	
}

TArray<ATileCpp*> AGridCpp::TryAnyMatch(ATileCpp* StartingTile, bool bMustMatchID /*= true*/)
{
	TArray<ATileCpp*> AllMatchingTiles;

	if (IsValid(StartingTile))
	{
		ATileCpp* NeighborTile;
		TArray<ATileCpp*> MatchInProgress;
		int RunLength = 10;
		
		if (RunLength < 0)
		{
			RunLength = MinimumRunLength;
		}

		// Handle special, trivial cases.
		
		if (RunLength == 0)
		{
			return AllMatchingTiles;
		}
		else if (RunLength == 1)
		{
			AllMatchingTiles.Add(StartingTile);
			return AllMatchingTiles;
		}

		// Check verticals, then check horizontals.
		for (int32 Horizontal = 0; Horizontal < 2; ++Horizontal)
		{
			MatchInProgress.Add(StartingTile);
			// Check negative direction, then check positive direction.
			for (int32 Direction = -1; Direction <= 1; Direction += 2)
			{
				int32 MaxGridOffset = !bMustMatchID ? RunLength : (Horizontal ? CurrGridWidth : CurrGridHeight);
				// Check run length. A run ends when we go off the edge of the map or hit a tile that doesn't match, provided we care about matching.
				for (int32 GridOffset = 1; GridOffset < MaxGridOffset; ++GridOffset)
				{
				int AddX = (Direction * (Horizontal ? GridOffset : 0));
				int AddY = (Direction * (Horizontal ? 0 : GridOffset));
				if(IsValid(GetTileByAdress(StartingTile->XAdress + AddX, StartingTile->YAdress + AddY)))
					//if (GetGridAddressWithOffset(StartingTile->GetGridAddress(), Direction * (Horizontal ? GridOffset : 0), Direction * (Horizontal ? 0 : GridOffset), NeighborGridAddress))
					{
						NeighborTile = GetTileByAdress(StartingTile->XAdress + AddX, StartingTile->YAdress + AddY);
						MatchInProgress.Add(NeighborTile);
						if (NeighborTile && HasMatchRef(MatchInProgress))
						{
							continue;
						}
						else
						{
							MatchInProgress.Remove(NeighborTile);
						}
						break;
					}
					else break;
				}
			}
			// See if we have enough to complete a run, or if matching wasn't required. If so, keep the tiles. Note that we add 1 to our match-in-progress because the starting tile isn't counted yet.
			if (!bMustMatchID || (MatchInProgress.Num() >= 3 ))
			{
				AllMatchingTiles.Append(MatchInProgress);
				break;
			}
			MatchInProgress.Empty();
		}
		// If we found any other tile, or if we're not concerned with matching TileID, then we know we have a valid run, and we need to add the original tile to the list.
		// If we do care about matching tile type and we haven't found anything by this point, then we don't have a match and should not return the starting tile in a list by itself.
		if ((AllMatchingTiles.Num() > 0) || !bMustMatchID)
		{
			//AllMatchingTiles.Add(StartingTile);
		}
		// check if we have match
		if (AllMatchingTiles.Num() > 2)
		{
			return AllMatchingTiles;
		}
		// try run again in single direction
		AllMatchingTiles.Empty();

		// Check verticals, then check horizontals.
		for (int32 Horizontal = 0; Horizontal < 2; ++Horizontal)
		{
			MatchInProgress.Add(StartingTile);
			// Check negative direction, then check positive direction.
			for (int32 Direction = -1; Direction <= 1; Direction += 2)
			{
				MatchInProgress.Empty();
				MatchInProgress.Add(StartingTile);
				int32 MaxGridOffset = !bMustMatchID ? RunLength : (Horizontal ? CurrGridWidth : CurrGridHeight);
				// Check run length. A run ends when we go off the edge of the map or hit a tile that doesn't match, provided we care about matching.
				for (int32 GridOffset = 1; GridOffset < MaxGridOffset; ++GridOffset)
				{
					int AddX = (Direction * (Horizontal ? GridOffset : 0));
					int AddY = (Direction * (Horizontal ? 0 : GridOffset));
					if (IsValid(GetTileByAdress(StartingTile->XAdress + AddX, StartingTile->YAdress + AddY)))
						//if (GetGridAddressWithOffset(StartingTile->GetGridAddress(), Direction * (Horizontal ? GridOffset : 0), Direction * (Horizontal ? 0 : GridOffset), NeighborGridAddress))
					{
						NeighborTile = GetTileByAdress(StartingTile->XAdress + AddX, StartingTile->YAdress + AddY);
						MatchInProgress.Add(NeighborTile);
						if (NeighborTile && HasMatchRef(MatchInProgress))
						{
							continue;
						}
						else
						{
							MatchInProgress.Remove(NeighborTile);
						}
						break;
					}
					else break;
				}
			}
			// See if we have enough to complete a run, or if matching wasn't required. If so, keep the tiles. Note that we add 1 to our match-in-progress because the starting tile isn't counted yet.
			if (!bMustMatchID || (MatchInProgress.Num() >= 3))
			{
				AllMatchingTiles.Append(MatchInProgress);
				break;
			}
			MatchInProgress.Empty();
		}
		// If we found any other tile, or if we're not concerned with matching TileID, then we know we have a valid run, and we need to add the original tile to the list.
		// If we do care about matching tile type and we haven't found anything by this point, then we don't have a match and should not return the starting tile in a list by itself.
		if ((AllMatchingTiles.Num() > 0) || !bMustMatchID)
		{
			//AllMatchingTiles.Add(StartingTile);
		}
	}
	return AllMatchingTiles;
}

void AGridCpp::MoveComplite(ATileCpp * Tile)
{
	if (MainState == EGridState::SwipeStarted)
	{
		SwipeEnd(Tile);
	}
	else if(MainState == EGridState::Gravity)
	{
		RespawnInvalid();
	}
	else if(MainState == EGridState::Duplicate)
	{
		//Gravity();
	}
	else if (MainState == EGridState::PreGravity)
	{
		Gravity();	
		MainState = EGridState::Gravity;
	}
}

void AGridCpp::SwipeEnd(ATileCpp * Tile)
{
	{
		if (Tile == SelectedTile)
		{
			SelectedEndMove = true;
		}
		else if (Tile == SecondTile)
		{
			SecondEndMove = true;
		}

		if (SelectedEndMove && SecondEndMove)
		{
			TArray<ATileCpp*> ArrTile;
			UE_LOG(LogTemp, Warning, TEXT("Try any match Selected"));
			ArrTile = TryAnyMatch(SelectedTile);
			if (ArrTile.Num() > 2)
			{
				MatchTiles(ArrTile);
				SelectedEndMove = false;
				SecondEndMove = false;
				return;
			}
			ArrTile.Empty();
			UE_LOG(LogTemp, Warning, TEXT("Try any match Second"));
			ArrTile = TryAnyMatch(SecondTile);
			if (ArrTile.Num() > 2)
			{
				MatchTiles(ArrTile);
				SelectedEndMove = false;
				SecondEndMove = false;
				return;
			}
			ATileCpp* SaveTile = SelectedTile;
			SelectedTile = SecondTile;
			StartSwipeElems(SaveTile);
			SelectedEndMove = false;
			SecondEndMove = false;
		}
	}
}
	

void AGridCpp::MoveTileToNewId(ATileCpp* Tile, int newX, int newY, bool ClearOldPos /*= true*/)
{
	if (IsValid(Tile))
	{	
		
		if (ClearOldPos)
		{
			Grid[Tile->XAdress][Tile->YAdress] = nullptr;
		}
		
		Tile->MoveToNewId(newX,newY);
		Tile->XAdress = newX;
		Tile->YAdress = newY;
		Grid[newX][newY] = Tile;
	}
}

void AGridCpp::GetTileXY(ATileCpp * Tile, int & X, int & Y)
{
	if (IsValid(Tile))
	{
		X = Tile->XAdress;
		Y = Tile->YAdress;
	}
}

void AGridCpp::MatchTiles(TArray<ATileCpp*> TilesRef)
{
	EMatchType Match = EMatchType::ColorAndForm;
	if (TilesRef.IsValidIndex(0))
	{
		UE_LOG(LogTemp, Warning, TEXT("MatchTiles"));
		FTileInfo StartInfo = TilesRef[0]->Info;
		for (int i = 0; i < TilesRef.Num(); ++i)
		{
			if (IsValid(TilesRef[i]))
			{
				if (TilesRef[i]->Info.Color != StartInfo.Color && Match == EMatchType::ColorAndForm)
				{
					Match = EMatchType::Form;
				}
				if (TilesRef[i]->Info.Form != StartInfo.Form && Match == EMatchType::ColorAndForm)
				{
					Match = EMatchType::Color;
				}
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("INVALID TILE IN MATCH"));
			}
		}
	}	
	int xMoveDir = 0;
	int yMoveDir = 0;
	if (IsValid(SelectedTile) && IsValid(SecondTile))
	{
		xMoveDir = SelectedTile->XAdress - SecondTile->XAdress;
		yMoveDir = SelectedTile->YAdress - SecondTile->YAdress;
	}
	switch (Match)
	{
	case EMatchType::Color:
		UE_LOG(LogTemp, Warning, TEXT("Color"));
		PrintToUI(FString::Printf(TEXT("Color %d"), TilesRef.Num()));
		PrintToUI(FString::Printf(TEXT("Destroy %d "), TilesRef.Num()));
		for (auto& It : TilesRef)
		{
			if (IsValid(It))
			{
				Grid[It->XAdress][It->YAdress] = nullptr;
				It->Destroy();
			}
		}
		Gravity();
		MainState = EGridState::Gravity;
		break;
	case EMatchType::Form:
		UE_LOG(LogTemp, Warning, TEXT("Form"));
		
		if (TilesRef.Num() == 3)
		{
			UE_LOG(LogTemp, Warning, TEXT("form 3"));
			PrintToUI(FString::Printf(TEXT("form %d"), TilesRef.Num()));
			AddPointsNear(TilesRef);
			PrintToUI(FString::Printf(TEXT("Destroy %d "), TilesRef.Num()));
			for (auto& It : TilesRef)
			{
				if (IsValid(It))
				{
					Grid[It->XAdress][It->YAdress] = nullptr;
					It->Destroy();
				}
			}
			Gravity();
			MainState = EGridState::Gravity;
		}

		if (TilesRef.Num() == 4)
		{
			UE_LOG(LogTemp, Warning, TEXT("form 4"));
			PrintToUI(FString::Printf(TEXT("form %d"), TilesRef.Num()));
			AddPointsNear(TilesRef);
			// if line Horizontal
			if (IsValid(TilesRef[0]) && IsValid(TilesRef[1]) && TilesRef[0]->YAdress == TilesRef[1]->YAdress)
			{
				if (TilesRef[0]->Info.Form == EFormTile::Cube)
				{
					ATileCpp* MoveTile = GetMinX(TilesRef);
					if (IsValid(MoveTile))
					{
						PrintToUI(FString::Printf(TEXT("Match cube move line left")));
						MoveLine(MoveTile->XAdress - 1, MoveTile->YAdress, 4, -1, 0);
						//MainState = EGridState::PreGravity;
					}
				}
				if (TilesRef[0]->Info.Form == EFormTile::Rhomb)
				{
					ATileCpp* MoveTile = GetMinX(TilesRef,false);
					if (IsValid(MoveTile))
					{
						PrintToUI(FString::Printf(TEXT("Match rhomb move line right")));
						MoveLine(MoveTile->XAdress + 1, MoveTile->YAdress, 4, 1, 0);
						//MainState = EGridState::PreGravity;
					}
				}	
				MainState = EGridState::PreGravity;
				if (TilesRef[0]->Info.Form == EFormTile::Triangle)
				{
					PrintToUI(FString::Printf(TEXT("Match triangle move line up")));
					for (auto& It : TilesRef)
					{
						if (IsValid(It))
						{											
							MoveLine(It->XAdress, It->YAdress + 1, 1, 0, 1);
						}
					}
					MainState = EGridState::Gravity;
				}

			}
			// if line vertical
			else
			{
				
				if (TilesRef[0]->Info.Form == EFormTile::Triangle) 
				{
					ATileCpp* MoveTile = GetMinY(TilesRef, false);
					if (IsValid(MoveTile))
					{
						PrintToUI(FString::Printf(TEXT("Match triangle move line up")));
						MoveLine(MoveTile->XAdress, MoveTile->YAdress + 1, 1, 0, 1);
					}
					MainState = EGridState::Gravity;
				}
				else //for cube and rhomb
				{
					PrintToUI(FString::Printf(TEXT("Match rhomb move line right")));
					for (auto& It : TilesRef)
					{
						if (IsValid(It))
						{
							if (TilesRef[0]->Info.Form == EFormTile::Cube)
							{
								MoveLine(It->XAdress - 1, It->YAdress, 4, -1, 0);
								PrintToUI(FString::Printf(TEXT("Match cube move line left")));
							}
							else if(TilesRef[0]->Info.Form == EFormTile::Rhomb)
							{ 
								PrintToUI(FString::Printf(TEXT("Match rhomb move line right"))); 
								MoveLine(It->XAdress + 1, It->YAdress, 4, 1, 0); 
							}
						}
					}
					MainState = EGridState::PreGravity;
				}				
			}
		
		}
		if (TilesRef.Num() >= 5)
		{
			UE_LOG(LogTemp, Warning, TEXT("form 5"));
			PrintToUI(FString::Printf(TEXT("form %d"), TilesRef.Num()));
			if (TilesRef[0]->Info.Form == EFormTile::Hexa) 
			{ 
				PrintToUI(FString::Printf(TEXT("Match hexa destroy grid")));
				DestroyGrid();

			};

			EFormTile StartForm = TilesRef[0]->Info.Form;
			PrintToUI(FString::Printf(TEXT("Upgrade form for %d "), TilesRef.Num()));
			for (int i = 0; i < TilesRef.Num(); i++)
			{
				if(StartForm == EFormTile::Triangle){TilesRef[i]->SetNewForm(EFormTile::Cube); continue;}
				else if (StartForm == EFormTile::Cube) {TilesRef[i]->SetNewForm(EFormTile::Rhomb); continue;}
				else if (StartForm == EFormTile::Rhomb) {TilesRef[i]->SetNewForm(EFormTile::Hexa); continue;};
			}
		}
		
		break;
	case EMatchType::ColorAndForm:
	
		if (TilesRef.Num() == 3)
		{
			UE_LOG(LogTemp, Warning, TEXT("Color and form 3"));
			PrintToUI(FString::Printf(TEXT("Color and form %d destroy"), TilesRef.Num()));
			AddPointsNear(TilesRef);
			for (auto& It : TilesRef)
			{
				if (IsValid(It))
				{
					Grid[It->XAdress][It->YAdress] = nullptr;
					It->Destroy();
				}
			}
			Gravity();
			MainState = EGridState::Gravity;
		}

		if (TilesRef.Num() == 4)
		{
			UE_LOG(LogTemp, Warning, TEXT("form 4"));
			PrintToUI(FString::Printf(TEXT("Color and form %d"), TilesRef.Num()));
			AddPointsNear(TilesRef);
			// if line Horizontal
			if (IsValid(TilesRef[0]) && IsValid(TilesRef[1]) && TilesRef[0]->YAdress == TilesRef[1]->YAdress)
			{
				if (TilesRef[0]->Info.Form == EFormTile::Cube)
				{
					ATileCpp* MoveTile = GetMinX(TilesRef);
					if (IsValid(MoveTile))
					{
						MoveLine(MoveTile->XAdress - 1, MoveTile->YAdress, 4, -1, 0);
						PrintToUI(FString::Printf(TEXT("match cube move line left"), MoveTile->YAdress));
						//MainState = EGridState::PreGravity;
					}
				}
				if (TilesRef[0]->Info.Form == EFormTile::Rhomb)
				{
					ATileCpp* MoveTile = GetMinX(TilesRef, false);
					if (IsValid(MoveTile))
					{
						PrintToUI(FString::Printf(TEXT("Match rhomb move line right")));
						MoveLine(MoveTile->XAdress + 1, MoveTile->YAdress, 4, 1, 0);
						//MainState = EGridState::PreGravity;
					}
				}
				MainState = EGridState::PreGravity;
				if (TilesRef[0]->Info.Form == EFormTile::Triangle)
				{
					PrintToUI(FString::Printf(TEXT("Match triangle move line up")));
					for (auto& It : TilesRef)
					{
						if (IsValid(It))
						{
							MoveLine(It->XAdress, It->YAdress + 1, 1, 0, 1);
						}
					}
					MainState = EGridState::Gravity;
				}

			}
			// if line vertical
			else
			{

				if (TilesRef[0]->Info.Form == EFormTile::Triangle)
				{
					ATileCpp* MoveTile = GetMinY(TilesRef, false);
					if (IsValid(MoveTile))
					{
						MoveLine(MoveTile->XAdress, MoveTile->YAdress + 1, 1, 0, 1);
					}
					MainState = EGridState::Gravity;
				}
				else //for cube and rhomb
				{
					for (auto& It : TilesRef)
					{
						if (IsValid(It))
						{
							if (TilesRef[0]->Info.Form == EFormTile::Cube) { MoveLine(It->XAdress - 1, It->YAdress, 4, -1, 0); }
							else if (TilesRef[0]->Info.Form == EFormTile::Rhomb) { MoveLine(It->XAdress + 1, It->YAdress, 4, 1, 0); }
						}
					}
					MainState = EGridState::PreGravity;
				}
			}
		}
		if (TilesRef.Num() >= 5)
		{
			UE_LOG(LogTemp, Warning, TEXT("Color and form 5"));
			PrintToUI(FString::Printf(TEXT("Color and form %d"), TilesRef.Num()));
			for (int i = 0; i < TilesRef.Num(); i++)
			{
				if (IsValid(TilesRef[i]))
				{
					if (TilesRef[i] == SelectedTile || TilesRef[i] == SecondTile)
					{
						PrintToUI(FString::Printf(TEXT("Spawnbomb"))); 
						SpawnBombAt(TilesRef[i]->XAdress, TilesRef[i]->YAdress,TilesRef[0]->Info.Form);
					}
					else
					{
						Grid[TilesRef[i]->XAdress][TilesRef[i]->YAdress] = nullptr;
						TilesRef[i]->Destroy();
					}					
				}
			}
			Gravity();
			MainState = EGridState::Gravity;
		}
		break;
	default:
		break;
	}
	if (IsValid(SelectedTile))
	{
		SelectedTile->TileDeselected();
	}
	SelectedTile = nullptr;
	SecondTile = nullptr;
}

ATileCpp* AGridCpp::GetMinY(TArray<ATileCpp *> TilesRef, bool min /*= true*/)
{
	ATileCpp* CurrTile = nullptr;
	int ly = 0;
	for (auto& It : TilesRef)
	{
		if (min ? It->YAdress < ly : It->YAdress > ly)
		{
			ly = It->YAdress;
			CurrTile = It;
		}
	}
	return CurrTile;
}

ATileCpp * AGridCpp::GetMinX(TArray<ATileCpp*> TilesRef, bool min)
{
	ATileCpp* CurrTile = nullptr;
	int lx = min ? 999 : 0;
	for (int i = 0; i < TilesRef.Num(); ++i)
	{
		if (min)
		{ 
			if (TilesRef[i]->XAdress < lx)
			{
				lx = TilesRef[i]->XAdress;
				CurrTile = TilesRef[i];
			}		
		}
		else
		{
			if (TilesRef[i]->XAdress > lx)
			{
				lx = TilesRef[i]->XAdress;
				CurrTile = TilesRef[i];
			}
		}	
	}
	return CurrTile;
}

bool AGridCpp::HasMatch(TArray<FTileInfo> inTiles)
{
	if (inTiles.IsValidIndex(0))
	{
		bool Result = true;
		FTileInfo StartTile = inTiles[0];
		for (int i = 1; i<inTiles.Num();++i)
		{
			if (StartTile.Color != inTiles[i].Color || !Result)
			{
				Result = false;
				break;
			}
		}
		if(Result){return true;};
		Result = true;
		for (int i = 1; i < inTiles.Num(); ++i)
		{
			if (StartTile.Form != inTiles[i].Form || !Result)
			{
				Result = false;
				break;
			}
		}
		return Result;
	}
	return false;
}

bool AGridCpp::GetGridAddressWithOffset(int32 InitialGridAddress, int32 XOffset, int32 YOffset, int32 &ReturnGridAddress) const
{
	int32 NewAxisValue;

	// Initialize to an invalid address.
	ReturnGridAddress = -1;

	// Check for going off the map in the X direction.
	check(CurrGridWidth > 0);
	NewAxisValue = (InitialGridAddress % CurrGridWidth) + XOffset;
	if (NewAxisValue != FMath::Clamp(NewAxisValue, 0, (CurrGridWidth - 1)))
	{
		return false;
	}

	// Check for going off the map in the Y direction.
	NewAxisValue = (InitialGridAddress / CurrGridWidth) + YOffset;
	if (NewAxisValue != FMath::Clamp(NewAxisValue, 0, (CurrGridHeight - 1)))
	{
		return false;
	}

	ReturnGridAddress = (InitialGridAddress + XOffset + (YOffset * CurrGridWidth));
	check(ReturnGridAddress >= 0);
	check(ReturnGridAddress < (CurrGridWidth * CurrGridHeight));
	return true;
}

bool AGridCpp::HasMatchRef(TArray<ATileCpp*> InTiles)
{
	TArray<FTileInfo> InfoTiles;
	for (auto& It : InTiles)
	{
		if (IsValid(It))
		{
			InfoTiles.Add(It->Info);
		}
		else
		{
			return false;
		}
	}
	return HasMatch(InfoTiles);
}

void AGridCpp::Gravity()
{
	//for x
	int XMax = GetMaxX();
	bool NotMoveTiles = true;
	for (int ix = xOffset; ix <= XMax + xOffset; ++ix)
	{
		// for y
		for (int iy = yOffset; iy <= yOffset + CurrGridWidth; ++iy)
		{
		//UE_LOG(LogTemp, Warning, TEXT("x%d y%d"), ix, iy);
			if (iy > 0 && IsValid(Grid[ix][iy]))
			{	
				int FallIndex = -1;
				for (int iDown = 1; iDown <= CurrGridWidth; iDown++)
				{
					if (iy - iDown >= 0 && IsValid(Grid[ix][iy - iDown]))
					{
						if (FallIndex != -1)
						{
							MoveTileToNewId(Grid[ix][iy],ix, FallIndex,true);
							NotMoveTiles = false;
							break;
						}
						break;
					}
					else
					{
					if(iy - iDown < GroundLevel){break;};
						FallIndex = iy - iDown;
						if (FallIndex == GroundLevel)
						{
							MoveTileToNewId(Grid[ix][iy], ix, FallIndex,true);
							NotMoveTiles = false;
							break;
						}
					}
				}
			}
		}
	}
	if (NotMoveTiles)
	{
		GetWorld()->GetTimerManager().SetTimer(EventTimer, this, &AGridCpp::RespawnInvalid, 0.5f, false);
	}
}

void AGridCpp::RespawnInvalid()
{
	for (int ix = xOffsetStart; ix <= xOffsetStart + XStart; ++ix)
	{
		for (int iy = yOffsetStart; iy <= yOffsetStart + YStart; ++iy)
		{
			if (!IsValid(Grid[ix][iy]))
			{
				SpawnTile(ix,iy,GetTileForXY(ix,iy));
			}
		}
	}
}

void AGridCpp::DuplicateTiles(TArray<ATileCpp*> InTiles,int xDirection, int yDirection)
{
	if (InTiles.IsValidIndex(0))
	{	
		MoveLine(InTiles[0]->XAdress, InTiles[0]->YAdress,1,xDirection,yDirection);
		SpawnTile(InTiles[0]->XAdress+ xDirection, InTiles[0]->YAdress +yDirection, InTiles[0]->Info);
		Grid[InTiles[0]->XAdress][InTiles[0]->YAdress] = InTiles[0];
		//MainState = EGridState::Duplicate;
	}
}

void AGridCpp::MoveLine(int xStart, int yStart, int Offset, int xDirection, int yDirection)
{
	TArray<ATileCpp*> WillMoveTiles;
	int MaxIteration = CurrGridHeight > CurrGridWidth ? CurrGridHeight : CurrGridWidth;
	for (int i = 0; i <= MaxIteration; ++i)
	{
		int xMove = xStart + (i * xDirection);
		int yMove = yStart + (i * yDirection);


		if (0 <= xMove && xMove <= 50 && 0 <= yMove && yMove <= 50 && IsValid(Grid[xMove][yMove]))
		{
			WillMoveTiles.Add(Grid[xMove][yMove]);
		}
	}
	int XMax = GetMaxX();
	for (int i = WillMoveTiles.Num()-1; i!=-1; --i)
	{
		xOffset = FMath::Clamp(xOffset > WillMoveTiles[i]->XAdress + Offset * xDirection ? WillMoveTiles[i]->XAdress + Offset * xDirection : xOffset, 0, 100);
		yOffset = yOffset > WillMoveTiles[i]->YAdress + Offset * yDirection ? WillMoveTiles[i]->YAdress + Offset * yDirection : yOffset;
		CurrGridHeight = CurrGridHeight < (XMax + Offset * xDirection) - xOffset ? (XMax + Offset * xDirection) - xOffset : CurrGridHeight;
		CurrGridWidth = CurrGridWidth < (WillMoveTiles[i]->YAdress + Offset * yDirection) - yOffset ? (WillMoveTiles[i]->YAdress + Offset * yDirection) - yOffset : CurrGridWidth;
		if (IsInArrRange(WillMoveTiles[i]->XAdress + Offset * xDirection, WillMoveTiles[i]->YAdress + Offset * yDirection))
		{
			MoveTileToNewId(WillMoveTiles[i], WillMoveTiles[i]->XAdress + Offset * xDirection, WillMoveTiles[i]->YAdress + Offset * yDirection,true);	
		}
		else
		{
			WillMoveTiles[i]->Destroy();
		}
	}
}

void AGridCpp::AddPointsNear(TArray<ATileCpp*> TileArr, int Count /*=1*/)
{
	for (auto& It : TileArr)
	{
		for (int i = -1; i <= 1; i += 2)
		{			
			if (IsValid(GetTileByAdress(It->XAdress, It->YAdress + i)) && !TileArr.Contains(GetTileByAdress(It->XAdress, It->YAdress + i)))
			{
				GetTileByAdress(It->XAdress, It->YAdress + i)->AddPoints(1.f);
			}
		}
		for (int i = -1; i <= 1; i += 2)
		{
			if (IsValid(GetTileByAdress(It->XAdress + i, It->YAdress)) && !TileArr.Contains(GetTileByAdress(It->XAdress + i, It->YAdress)))
			{
				GetTileByAdress(It->XAdress + i, It->YAdress)->AddPoints(1.f);
			}
		}
	}
}

void AGridCpp::SpawnBombAt(int x, int y, EFormTile FromType /*= EFormTile::Triangle*/)
{	
	if (IsValid(GetTileByAdress(x, y)))
	{
		GetTileByAdress(x, y)->Destroy();
		Grid[x][y] = nullptr;
	}
	FActorSpawnParameters SpawnInfo;
	FTileInfo NewTileInfo;
	NewTileInfo.Form = EFormTile::Bomb;
	ATileCpp* CurrTile = GetWorld()->SpawnActor<ATileCpp>(TileClass, FVector(x*10.f, y*-10.f, GetActorLocation().Z), FRotator::ZeroRotator, SpawnInfo);
	CurrTile->Info = NewTileInfo;
	CurrTile->GridOwner = this;
	CurrTile->XAdress = x;
	CurrTile->YAdress = y;
	CurrTile->BombType = FromType == EFormTile::Triangle ? EBombType::Base : FromType == EFormTile::Cube ? EBombType::Vertical : EBombType::Horizontal;
	Grid[x][y] = CurrTile;
	//SetTileInAdress(CurrTile,x,y);
}

TArray<ATileCpp*> AGridCpp::GetNeibors(int x, int y)
{
	TArray<ATileCpp*> CurrArr = TArray<ATileCpp*>();
	for (int ix = -1; ix <= 1; ix += 2)
	{
		if (IsValid(GetTileByAdress(x + ix, y)))
		{
			CurrArr.Add(GetTileByAdress(x + ix, y));
		}
		for (int iy = -1; iy <= 1; iy += 2)
		{
			if (IsValid(GetTileByAdress(x + ix, y + iy)))
			{
				CurrArr.Add(GetTileByAdress(x + ix, y + iy));
			}
			if (IsValid(GetTileByAdress(x , y + iy)))
			{
				CurrArr.Add(GetTileByAdress(x , y + iy));
			}
		}
	}
	return CurrArr;
}

void AGridCpp::ExecuteBomb(int x, int y, int xDirection, int yDirection, ATileCpp* BombTile /*= nullptr*/)
{
	if(!IsValid(BombTile)){return;};
TArray<ATileCpp*> ArrToDestr = GetNeibors(x, y);
	switch (BombTile->BombType)
	{
	default:
		break;
	case EBombType::Base:
	
		for (auto& It : ArrToDestr)
		{
			if (IsValid(It))
			{
				Grid[It->XAdress][It->YAdress] = nullptr;
				It->Destroy();
			}
		}
		break;
	case EBombType::Horizontal:
		for (int i = 0; i < 100; i++)
		{
			if(IsValid(Grid[i][y]))
			{
				Grid[i][y]->Destroy();
				Grid[i][y] = nullptr;
			}
		}
		break;
	case EBombType::Vertical:
		for (int i = 0; i < 100; i++)
		{
			if (IsValid(Grid[x][i]))
			{
				Grid[x][i]->Destroy();
			}
		}
		GetWorld()->GetTimerManager().SetTimer(EventTimer,this, &AGridCpp::RespawnInvalid,0.5f,false);
		break;
	}

	Gravity();
	MainState = EGridState::Gravity;
}

void AGridCpp::DestroyGrid()
{
	for (int x = 0; x <= 100; x++)
	{
		for (int y = 0; y <= 100; y++)
		{
			if (IsValid(GetTileByAdress(x, y))) 
			{
				GetTileByAdress(x,y)->Destroy();
				Grid[x][y] = nullptr;
			}
		}
	}
	GenerateGrid(XStart,YStart, CurrGridWidth,CurrGridHeight);
	for (int x = 0; x <= 100; x++)
	{
		for (int y = 0; y <= 100; y++)
		{
			if (IsValid(GetTileByAdress(x, y)))
			{
				GetTileByAdress(x, y)->AddPoints(1);
			}
		}
	}
}

bool AGridCpp::CanSwipeTiles(ATileCpp * FirstTile, ATileCpp * SecTile)
{
	if (IsValid(FirstTile) && IsValid(SecTile))
	{
		int fX = FirstTile->XAdress;
		int fY = FirstTile->YAdress;


		int sX = SecTile->XAdress;
		int sY = SecTile->YAdress;

		return fX + 1 == sX && fY == sY || fX - 1 == sX &&  fY == sY || fX == sX && fY + 1 == sY || fX == sX && fY - 1 == sY;
	}
	return false;
}

bool AGridCpp::IsInArrRange(int x, int y)
{
	return 0 <= x && x <= 50 && 0 <= y && y <= 50;
}

int AGridCpp::GetMaxX()
{
	for (int i = 100; i >= 0; i--)
	{

		if (IsInArrRange(i, YStart) && IsValid(Grid[i][YStart]))
		{
			return i;
		}
	}
	return 0;
}


void AGridCpp::PrintToUI_Implementation(const FString& Print)
{
	
}

