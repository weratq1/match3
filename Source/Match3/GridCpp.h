// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Materials/Material.h"
#include "GameFramework/Actor.h"

#include "GridCpp.generated.h"

class ATileCpp;


UENUM(BlueprintType)
enum class EColorTile : uint8{
	Red,
	Blue,
	Yellow,
	Green
};

UENUM(BlueprintType)
enum class EBombType : uint8
{
	Base,
	Horizontal,
	Vertical
};

UENUM(BlueprintType)
enum class EFormTile : uint8
{
	Triangle,
	Cube,
	Rhomb,
	Hexa,
	Bomb
};

USTRUCT(BlueprintType)
struct FTileInfo
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	EColorTile Color;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	EFormTile Form;
};

UENUM(BlueprintType)
enum class EGridState : uint8 
{
	Idle,
	SwipeStarted,
	Gravity,
	Duplicate,
	PreGravity
};

UENUM(BlueprintType)
enum class EMatchType : uint8
{
	Color,
	Form,
	ColorAndForm
};

UCLASS()
class MATCH3_API AGridCpp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGridCpp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<EColorTile> ListColors;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<EFormTile> ListForm;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ATileCpp> TileClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EGridState MainState;

	UPROPERTY(BlueprintReadWrite)
	ATileCpp* SelectedTile;

	UPROPERTY(BlueprintReadWrite)
		ATileCpp* SecondTile;
	//x
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int CurrGridWidth;
	int XStart;
	int xOffset;
	int xOffsetStart;

	
	//y
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int CurrGridHeight;
	int YStart;
	int yOffset;
	int yOffsetStart;

	int GroundLevel;

	int MinimumRunLength;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool SelectedEndMove;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool SecondEndMove;

	
	ATileCpp* Grid[100][100];
	UPROPERTY(BlueprintReadWrite)
	TArray<ATileCpp*> GridTiles;

	UFUNCTION(BlueprintCallable)
	ATileCpp* GetTileByAdress(int x, int y);

	UFUNCTION(BlueprintCallable)
	void SetTileInAdress(ATileCpp* Tile, int x, int y);

	UFUNCTION(BlueprintCallable)
	void GenerateGrid(int StartX, int StartY, int LengthX, int LengthY);

	UFUNCTION(BlueprintCallable)
	FTileInfo GetTileForXY(int x, int y);

	UFUNCTION(BlueprintCallable)
	bool IsDiffrentTiles(TArray<FTileInfo> Tiles, bool UseColor = true, bool UseForm = true);

	UFUNCTION(BlueprintCallable)
	FTileInfo GenerateRandomTile();

	UFUNCTION(BlueprintCallable)
	void SpawnTile(int x, int y, FTileInfo InInfo);

	UFUNCTION(BlueprintCallable)
	void OnTileSelected(ATileCpp* Tile);

	UFUNCTION(BlueprintCallable)
	void StartSwipeElems(ATileCpp* SecondElem);

	UFUNCTION(BlueprintCallable)
	TArray<ATileCpp*> TryAnyMatch(ATileCpp* StartingTile, bool bMustMatchID = true);

	UFUNCTION(BlueprintCallable)
	void MoveComplite(ATileCpp* Tile);

	void SwipeEnd(ATileCpp * Tile);


	UFUNCTION(BlueprintCallable)
	void MoveTileToNewId(ATileCpp* Tile,int newX,int newY, bool ClearOldPos = true);

	UFUNCTION(BlueprintCallable)
	void GetTileXY(ATileCpp* Tile, int& X, int& Y);

	UFUNCTION(BlueprintCallable)
	void MatchTiles(TArray<ATileCpp*> TilesRef);

	ATileCpp* GetMinY(TArray<ATileCpp *> TilesRef, bool min = true);
	ATileCpp* GetMinX(TArray<ATileCpp *> TilesRef, bool min = true);

	UFUNCTION(BlueprintCallable)
	bool HasMatch(TArray<FTileInfo> inTiles);

	bool GetGridAddressWithOffset(int32 InitialGridAddress, int32 XOffset, int32 YOffset, int32 & ReturnGridAddress) const;

	UFUNCTION(BlueprintCallable)
	bool HasMatchRef(TArray<ATileCpp*> InTiles);

	void Gravity();

	void RespawnInvalid();

	void DuplicateTiles(TArray<ATileCpp*> InTiles,int xDirection,int yDirection);

	void MoveLine(int xStart, int yStart, int Offset, int xDirection, int yDirection);

	void AddPointsNear(TArray<ATileCpp*> TileArr, int Count = 1);

	void SpawnBombAt(int x, int y, EFormTile FromTile = EFormTile::Triangle);

	TArray<ATileCpp*> GetNeibors(int x, int y);

	//if xDirection and yDirection == 0 will destroy near tiles
	void ExecuteBomb(int x, int y, int xDirection, int yDirection, ATileCpp* BombTile = nullptr);

	void DestroyGrid();

	bool CanSwipeTiles(ATileCpp* FirstTile, ATileCpp* SecTile);

	bool IsInArrRange(int x, int y);

	int GetMaxX();

	FTimerHandle EventTimer;
	//void TextToString(const wchar_t Test);

	UFUNCTION(BlueprintNativeEvent)
	void PrintToUI(const FString& Print);

};
