// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridCpp.h"
#include "TileCpp.generated.h"




UCLASS()
class MATCH3_API ATileCpp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATileCpp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTileInfo Info;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int XAdress;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int YAdress;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Points;
	UFUNCTION(BlueprintNativeEvent)
	void AddPoints(float Count);

	UPROPERTY(BlueprintReadWrite)
	AGridCpp* GridOwner;

	UFUNCTION(BlueprintCallable)
	void SelectTile();
	UFUNCTION(BlueprintImplementableEvent)
	void TileSelected();
	UFUNCTION(BlueprintImplementableEvent)
	void TileDeselected();
	UFUNCTION(BlueprintImplementableEvent)
	void RefreshInfo();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void MoveToNewId(int x, int y);

	UPROPERTY(BlueprintReadWrite)
	EBombType BombType;

	UFUNCTION(BlueprintCallable)
		void MoveEnd(ATileCpp* Tile);

	void SetNewForm(EFormTile NewForm);

	
};
